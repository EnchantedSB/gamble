package me.enchanted.gamble.games.slots;

import org.bukkit.Instrument;
import org.bukkit.Location;
import org.bukkit.Note;
import org.bukkit.entity.Player;

import me.enchanted.gamble.Gamble;
import me.enchanted.gamble.games.Slots;

public class ResultsTask implements Runnable {

	private Slots game;
	private int task;
	
	public ResultsTask(Slots game, Integer task) {
		this.game = game;
		this.task = task;
	}

	@Override
	public void run() {

		double multi = 0;

		if (game.getInv().getItem(2).getType().equals(game.getInv().getItem(4).getType())
				&& game.getInv().getItem(2).getType().equals(game.getInv().getItem(6).getType())) {
			multi += Gamble.getInstance().getSlotsCache().getMultiplier(game.getInv().getItem(2).getType());
		}
		if (game.getInv().getItem(11).getType().equals(game.getInv().getItem(13).getType())
				&& game.getInv().getItem(11).getType().equals(game.getInv().getItem(15).getType())) {
			multi += Gamble.getInstance().getSlotsCache().getMultiplier(game.getInv().getItem(11).getType());

		}
		if (game.getInv().getItem(20).getType().equals(game.getInv().getItem(22).getType())
				&& game.getInv().getItem(20).getType().equals(game.getInv().getItem(24).getType())) {

			multi += Gamble.getInstance().getSlotsCache().getMultiplier(game.getInv().getItem(20).getType());
		}

		game.scheduler.cancelTask(task);
		game.getPlayer().sendMessage("You got a multi of " + multi);
		
		if(multi > 0) {
			playVictoryMusic();
		} else {
			
		}
	}
	
	private void playVictoryMusic() {
        Player p = this.game.getPlayer();
        Location loc = p.getLocation();
        this.game.scheduler.scheduleSyncDelayedTask(Gamble.getInstance(), (Runnable)new Runnable() {
            @Override
            public void run() {
                p.playNote(loc, Instrument.PIANO, new Note((byte)0, Note.Tone.C, true));
            }
        }, 10L);
        this.game.scheduler.scheduleSyncDelayedTask(Gamble.getInstance(), (Runnable)new Runnable() {
            @Override
            public void run() {
                p.playNote(loc, Instrument.PIANO, new Note((byte)1, Note.Tone.F, true));
            }
        }, 13L);
        this.game.scheduler.scheduleSyncDelayedTask(Gamble.getInstance(), (Runnable)new Runnable() {
            @Override
            public void run() {
                p.playNote(loc, Instrument.PIANO, new Note((byte)1, Note.Tone.A, true));
            }
        }, 16L);
        this.game.scheduler.scheduleSyncDelayedTask(Gamble.getInstance(), (Runnable)new Runnable() {
            @Override
            public void run() {
                p.playNote(loc, Instrument.PIANO, new Note((byte)0, Note.Tone.C, true));
            }
        }, 21L);
        this.game.scheduler.scheduleSyncDelayedTask(Gamble.getInstance(), (Runnable)new Runnable() {
            @Override
            public void run() {
                p.playNote(loc, Instrument.PIANO, new Note((byte)1, Note.Tone.F, true));
            }
        }, 24L);
        this.game.scheduler.scheduleSyncDelayedTask(Gamble.getInstance(), (Runnable)new Runnable() {
            @Override
            public void run() {
                p.playNote(loc, Instrument.PIANO, new Note((byte)1, Note.Tone.A, true));
            }
        }, 27L);
    }
}
