package me.enchanted.gamble.games.slots;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.enchanted.gamble.Gamble;
import me.enchanted.gamble.games.Slots;

public class RotateTask implements Runnable
{
    private Slots game;
    private Integer i;
    
    public RotateTask(final Slots game, final Integer i) {
        this.game = game;
        this.i = i;
    }
    
    @Override
    public void run() {
        this.rotateColumn(this.i);
    }
    
    private void rotateColumn(final Integer column) {
        final ArrayList<Material> last = new ArrayList<Material>();
        last.add(game.getInv().getItem(column+9).getType());
        last.add(game.getInv().getItem(column).getType());
        Material id;
        for (id = this.getNext(); id == last.get(0); id = this.getNext()) {}
        game.getInv().setItem(column, new ItemStack(id));
        game.getInv().setItem(column + 18, new ItemStack(last.get(0)));
        game.getInv().setItem(column + 9, new ItemStack(last.get(1)));
    }
    
    private Material getNext() {
        return Gamble.getInstance().getSlotsCache().getRandomItem();
    }
}
