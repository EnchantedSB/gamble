package me.enchanted.gamble.games.slots;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.enchanted.gamble.utils.NumberUtils;

public class SlotsMainGUI {

	private Inventory inv;
	private Player p;
	private double amount = 0;

	public SlotsMainGUI(Player p, double a) {
		this.p = p;
		this.amount = a;
	}

	public void open() {
		this.inv = Bukkit.createInventory(null, 27, "�4�lSlots");

		int g = 1;
		for (int i = 0; i < inv.getSize(); i++) {

			ItemStack is = null;

			if (g % 2 == 0) {
				is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 3);
			} else {
				is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 0);
			}

			g++;

			ItemMeta im = is.getItemMeta();
			im.setDisplayName("�7");
			is.setItemMeta(im);

			inv.setItem(i, is);

		}

		ItemStack amount = new ItemStack(Material.PAPER);
		ItemMeta amountM = amount.getItemMeta();
		amountM.setDisplayName("�cClick to Enter Amount");
		if (this.amount == 0) {
			amountM.setLore(Arrays.asList("�7", "�7Click this paper to enter", "�7the amount of money you",
					"�7are betting on slots."));
		} else {
			amountM.setLore(Arrays.asList("�7", "�7Entered amount: �8" + NumberUtils.withSuffix(this.amount)));
		}
		amount.setItemMeta(amountM);
		

		ItemStack spin = new ItemStack(Material.LEVER);
		ItemMeta spinM = spin.getItemMeta();
		if (this.amount == 0) {
			spinM.setDisplayName("�cPlease enter an amount");
		} else {
			spinM.setDisplayName("�eClick to Spin");
		}
		spin.setItemMeta(spinM);

		inv.setItem(11, amount);
		inv.setItem(13, spin);
		
		p.openInventory(inv);
	}

}
