package me.enchanted.gamble.games.slots;

import org.bukkit.Instrument;
import org.bukkit.Note;

import me.enchanted.gamble.games.Slots;

public class StopRotateTask implements Runnable {
	
	private Slots game;
	private Integer task;
	
	public StopRotateTask(Slots game, Integer task) {
		this.game = game;
		this.task = task;
	}
	
	@Override
	public void run() {
		this.game.scheduler.cancelTask(task);
	}

}
