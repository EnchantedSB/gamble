package me.enchanted.gamble.games.slots;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import me.enchanted.gamble.games.Slots;

public class GlassTask implements Runnable {

	private Slots game;
	int i = 0;

	public GlassTask(Slots game) {
		this.game = game;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		for (int i = 0; i < game.getInv().getSize(); i++) {
			if (game.getInv().getItem(i).getData().getData() == 3 && game.getInv().getItem(i).getType().equals(Material.STAINED_GLASS_PANE)) {
				System.out.print("h");
				game.getInv().setItem(i, new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 0));
			} else if (game.getInv().getItem(i).getData().getData() == 0 && game.getInv().getItem(i).getType().equals(Material.STAINED_GLASS_PANE)) {
				game.getInv().setItem(i, new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 3));
			}
		}

		i++;
	}
}
