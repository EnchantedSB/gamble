package me.enchanted.gamble.games.slots;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.Material;

import me.enchanted.gamble.Gamble;

public class SlotsCache {
	
	private static ThreadLocalRandom random = ThreadLocalRandom.current();
	private static List<Material> items = new ArrayList<Material>();
	private static Map<Material, Double> multi = new HashMap<Material, Double>();
	
	public SlotsCache() {
		for(String m : Gamble.getInstance().getConfig().getConfigurationSection("blocks").getKeys(false)) {
			for(int i = 0; i < Gamble.getInstance().getConfig().getInt("blocks." + m + ".chance"); i++) {
				items.add(Material.getMaterial(m.toUpperCase()));
			}
			multi.put(Material.getMaterial(m.toUpperCase()), Gamble.getInstance().getConfig().getDouble("blocks." + m + ".multiplier"));
		}
	}
	
	public Material getRandomItem() {
		return items.get(random.nextInt(items.size()));
	}
	
	public double getMultiplier(Material mat) {
		return multi.get(mat);
	}
}
