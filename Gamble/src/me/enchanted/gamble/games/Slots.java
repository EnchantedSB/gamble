package me.enchanted.gamble.games;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.Bukkit;
import org.bukkit.Instrument;
import org.bukkit.Material;
import org.bukkit.Note;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitScheduler;

import me.enchanted.gamble.Gamble;
import me.enchanted.gamble.games.slots.GlassTask;
import me.enchanted.gamble.games.slots.ResultsTask;
import me.enchanted.gamble.games.slots.RotateTask;
import me.enchanted.gamble.games.slots.StopRotateTask;

public class Slots {

	private TreeMap<Integer, ItemStack> slots = new TreeMap<Integer, ItemStack>();
	private Inventory inv;
	public BukkitScheduler scheduler;
	private Player p;
	private double amount;

	public Slots(Player p, Double amount) {
		this.p = p;
		this.amount = amount;

		this.scheduler = Gamble.getInstance().getServer().getScheduler();
		this.inv = Bukkit.createInventory(null, 27, "§4§lSpinning (Slots)");
		final Long[] delay = { 60L, 80L, 100L };

		int g = 1;
		for (int i = 0; i < inv.getSize(); i++) {

			
			ItemStack is = null;

			if (g % 2 == 0) {
				is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 3);
			} else {
				is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 0);
			}
			
			g++;
			
			ItemMeta im = is.getItemMeta();
			im.setDisplayName("§7");
			is.setItemMeta(im);
			
			inv.setItem(i, is);

		}

		int slot = 2;
		for (int i = 1; i < 10; i++) {
			ItemStack is = new ItemStack(getRandomItem());
			slots.put(slot, is);

			inv.setItem(slot, is);

			if (slot % 3 == 0) {
				slot += 5;
			} else {
				slot += 2;
			}
		}

		p.openInventory(inv);

		new BukkitRunnable() {
			
			int i = 5;
			@Override
			public void run() {
				p.playNote(p.getLocation(), Instrument.PIANO, new Note(i));
				if(i == 18) cancel();
				i++;
			}
		}.runTaskTimer(Gamble.getInstance(), 7, 7);
		
		final Integer[] task = new Integer[3];
		for (int i = 1; i < 4; i++) {
			task[i - 1] = this.scheduler.scheduleSyncRepeatingTask(Gamble.getInstance(), new RotateTask(this, 2 * i),
					0L, 3L);
			this.scheduler.scheduleSyncDelayedTask(Gamble.getInstance(), new StopRotateTask(this, task[i - 1]),
					(long) delay[3 - i]);
		}
		int glass = this.scheduler.scheduleSyncRepeatingTask(Gamble.getInstance(), new GlassTask(this), 10, 10);
		this.scheduler.scheduleSyncDelayedTask(Gamble.getInstance(), new ResultsTask(this, glass), (long) delay[2] + 1);
	}

	public TreeMap<Integer, ItemStack> getSlots() {
		return this.slots;
	}

	public Player getPlayer() {
		return this.p;
	}
	
	public double getAmount() {
		return this.amount;
	}

	@SuppressWarnings("serial")
	public Material getRandomItem() {
		List<Material> items = new ArrayList<Material>() {
			{
				add(Material.IRON_BLOCK);
				add(Material.DIAMOND_BLOCK);
				add(Material.EMERALD_BLOCK);
				add(Material.GOLD_BLOCK);
			}
		};

		return items.get(ThreadLocalRandom.current().nextInt(items.size() - 1));
	}

	public Inventory getInv() {
		return this.inv;
	}

}
