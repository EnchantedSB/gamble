package me.enchanted.gamble.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.enchanted.gamble.games.slots.SlotsMainGUI;

public class SlotsCommand implements CommandExecutor {
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		Player p = (Player) sender;
		
		SlotsMainGUI smg = new SlotsMainGUI(p, 0);
		
		smg.open();
		
		return true;
	}

}
