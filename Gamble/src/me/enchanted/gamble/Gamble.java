package me.enchanted.gamble;

import org.bukkit.plugin.java.JavaPlugin;

import me.enchanted.gamble.commands.SlotsCommand;
import me.enchanted.gamble.games.slots.SlotsCache;
import me.enchanted.gamble.hooks.VaultEco;
import me.enchanted.gamble.listeners.SlotsGUIClick;

public class Gamble extends JavaPlugin {

	private static Gamble instance;
	private static SlotsCache slotsCache;
	private VaultEco eco;

	public void onEnable() {
		instance = this;
		this.eco = new VaultEco(this);

        if (!this.eco.setupEconomy()) {
            this.getLogger().warning("Could not establish a hook to your economy plugin! EZPrestige requires you have Vault and a compatible economy plugin installed!");
            this.setEnabled(false);
            return;
        }
		
		saveDefaultConfig();
		
		slotsCache = new SlotsCache();
		setupCache();
		
		getCommand("test").setExecutor(new SlotsCommand());

		getServer().getPluginManager().registerEvents(new SlotsGUIClick(), this);
	}

	public static Gamble getInstance() {
		return instance;
	}

	public void setupCache() {
	}
	
	public SlotsCache getSlotsCache() {
		return slotsCache;
	}
	
	public VaultEco getEco() {
		return this.eco;
	}

}