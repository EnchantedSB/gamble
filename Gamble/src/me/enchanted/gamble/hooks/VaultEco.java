package me.enchanted.gamble.hooks;

import me.enchanted.gamble.Gamble;

import org.bukkit.plugin.*;
import org.bukkit.*;
import net.milkbowl.vault.economy.*;

public class VaultEco
{
    private Gamble plugin;
    public static Economy econ;
    
    static {
        VaultEco.econ = null;
    }
    
    public VaultEco(Gamble instance) {
        this.plugin = instance;
    }
    
    public boolean setupEconomy() {
        if (this.plugin.getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        final RegisteredServiceProvider<Economy> rsp = (RegisteredServiceProvider<Economy>)this.plugin.getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        VaultEco.econ = (Economy)rsp.getProvider();
        return VaultEco.econ != null;
    }
    
    public boolean hasEnoughMoney(final double amount, final OfflinePlayer p) {
        return VaultEco.econ.has(p, amount);
    }
    
    public boolean withdrawMoney(final double amount, final OfflinePlayer p) {
        final EconomyResponse econResp = VaultEco.econ.withdrawPlayer(p, amount);
        return econResp.transactionSuccess();
    }
    
    public double getBalance(final OfflinePlayer p) {
        return VaultEco.econ.getBalance(p);
    }
}
