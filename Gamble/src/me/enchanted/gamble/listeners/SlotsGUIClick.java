package me.enchanted.gamble.listeners;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.Inventory;

import me.enchanted.gamble.games.Slots;
import me.enchanted.gamble.games.slots.SlotsMainGUI;

public class SlotsGUIClick implements Listener {

	List<UUID> chat = new ArrayList<UUID>();
	Map<UUID, Double> amount = new HashMap<UUID, Double>();

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		if (e.getInventory() == null)
			return;

		Inventory inv = e.getInventory();

		if (e.getClickedInventory() == null)
			return;
		if (inv.getTitle() == null)
			return;

		Player p = (Player) e.getWhoClicked();
		UUID u = p.getUniqueId();

		if (inv.getTitle().equals("�4�lSlots")) {
			
			if (e.getSlot() == 11) {
				
				p.closeInventory();
				p.sendMessage("�6Please type the amount you would like to deposit in the slots.");
				chat.add(u);
				
			} else if (e.getSlot() == 13) {
				
				if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�cPlease enter an amount")) {
					p.playSound(p.getLocation(), Sound.ANVIL_LAND, 1, 1);
				} else if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("�eClick to Spin")) {
					new Slots(p, amount.get(u).doubleValue());
				}
				
			}

			e.setCancelled(true);
		} else if (inv.getTitle().equals("�4�lSpinning (Slots)")) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		Player p = e.getPlayer();
		UUID u = p.getUniqueId();

		if (chat.contains(u)) {
			try {
				
				double a = Double.valueOf(e.getMessage());
				
				if(a < 1) {
					p.sendMessage("�cPlease enter a valid number.");
					return;
				}
				
				amount.put(u, a);
				chat.remove(u);
				
				SlotsMainGUI smg = new SlotsMainGUI(p, a);
				smg.open();
				
			} catch (NumberFormatException ex) {
				p.sendMessage("�cPlease enter a valid number.");
			}
			e.setCancelled(true);
		}
	}

}
