package me.enchanted.gamble.utils;

public class NumberUtils {

	public static String withSuffix(double d) {
		if (d < 0)
			d = -d;
		if (d < 1000)
			return "" + d;
		int exp = (int) (Math.log(d) / Math.log(1000));
		return String.format("%.2f%c", d / Math.pow(1000, exp), "kMBTqQsS".charAt(exp - 1));
	}
}
